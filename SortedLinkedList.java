/**
 * @author Ryan Matson
 */
import java.util.*;
import java.io.*;

public class SortedLinkedList{
    private SortedNode head;
    private int count = 0;

    public SortedLinkedList(){
    }
    /**
     * Adds a double value to a node, then adds the node to the list in sorted order
     * @param input the data being added to the list
     * @return true if the data is added, false if the data is not added
     */

    public boolean append(double input){
        SortedNode sorted = new SortedNode(input, null);
        if(head==null){
            head = sorted;
            count++;
            return true;
        }
        SortedNode curr = head;

        if(head!=null && head.getNext()==null){
            if(head.getData()>input){
                SortedNode temp = head;
                head = sorted;
                head.setLink(temp);
                count++;
                return true;
            }
            else if (head.getData()<=input){
                head.setLink(sorted);
                count++;
                return true;
            }
        }
        for(int i=0; i<count; i++){
            SortedNode next = curr.getNext();
            if(curr.getData()<=input && next==null){
                curr.setLink(sorted);
                count++;
                return true;
            }
            if(curr.getData()<=input && input<=next.getData()){
                SortedNode temp = next;
                curr.setLink(sorted);
                curr.getNext().setLink(temp);
                count++;
                return true;
            }  
            if(i==0 && head.getData()>input){
                SortedNode temp = head;
                head = sorted;
                sorted.setLink(temp);
                count++;
                return true;
            }    
            curr = curr.getNext();
        }
        return false;    
    }
    /**
     * Removes an element at a specified position in the linkedlist.
     * @param pos the position of the element to be removed
     * @return true if the element is successfully removed, false if the element is not present in the list.
     */

    public boolean remove(int pos){
        if(pos<0 || pos>=count){
            throw new IndexOutOfBoundsException("This position is out of bounds.");
        }
        SortedNode current = head;
        if(pos==0){
            head = head.getNext();
            count--;
            return true;
        }
        if(current!=null){
            for(int i=1; i<pos-1; i++){
                if(current.getNext()==null){
                    return false;
                }
                current = current.getNext();
            }
            current.appendNext(current.getNext().getNext());
            count--;
            return true;
        }
        return false;
    }
    /**
     * Returns the number of elements in this list
     * @return the number of elements in the SortedLinkedList
     */

    public int getSize(){
        return count;
    }
    /**
     * Converts the SortedLinkedList into a string
     */
    public String toString(){
        SortedNode current = head;
        String out = "[";
        for(int i=0; i<count; i++){
            out = out+current.getData();
            if(current.getNext()!=null){
                out+=" -> ";
                current=current.getNext();
            }
        } 
        out+="]";
        return out; 
    }
    
}

