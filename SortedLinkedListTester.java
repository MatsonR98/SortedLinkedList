import java.util.*;
import java.io.*;

public class SortedLinkedListTester{
    public static void main(String []args){
        SortedLinkedList list = new SortedLinkedList();
        Random random = new Random(2);
        for(int i=0; i<20; i++){
            double d = random.nextDouble();
            list.append(d);
        }

        System.out.println(list);
    }
}