public class SortedNode{
    private double _data = 0.0;
    private SortedNode _link;

    public SortedNode(double data, SortedNode link){
        _data = data;
        _link = link;
    }

    public SortedNode(SortedNode link){
        _link = link;
    }


    public void setData(double data){
        _data = data;
    }

    public void setLink(SortedNode link){
        _link = link;
    }

    public double getData(){
        return _data;
    }

    public SortedNode getNext(){
        return _link;
    }

    public void appendNext(SortedNode next){
        _link = next;
    }

    public String toString(){
        return _data + "->";
    }

}